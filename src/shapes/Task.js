import PropTypes from 'prop-types'

export default PropTypes.shape({
  key: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string,
  imageUrl: PropTypes.string,
  favorite: PropTypes.bool,
  createdAt: PropTypes.number,
  updatedAt: PropTypes.number,
})
