import { uniqueId } from 'lodash'
import { all, fork, put, call, take } from 'redux-saga/effects'
import exportToFile from '../../utils/exportDataToFile'
import { actionTypes as exportDataActionsTypes, actions as exportDataActions } from '../modules/exportData'

function* exportDataToFile() {
  while (true) {
    const task = yield take(exportDataActionsTypes.EXPORT_DATA_TO_FILE)

    try {
      yield call(exportToFile, task.payload, `LiteTaskManagerExport_${Date.now()}_${uniqueId()}`)
      yield put(exportDataActions.exportDataToFileSuccess())
    } catch (error) {
      console.error(error)
      yield put(exportDataActions.exportDataToFileError(error))
    }
  }
}

export default function* user() {
  yield all([fork(exportDataToFile)])
}
