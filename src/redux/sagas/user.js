import { all, fork, put, call, take, cancel } from 'redux-saga/effects'
import * as userApi from '../../api/user'
import { actionTypes as userActionsTypes, actions as userActions } from '../modules/user'

function* authorize() {
  yield put(userActions.authPrepare())
  yield call(userApi.generateAuthStateChangePromise)
  let userProfile = yield call(userApi.getUserProfile)

  yield put(userActions.authPrepareSuccess())

  if (!userProfile) {
    try {
      const action = yield take([userActionsTypes.LOGIN])
      yield call(userApi.login, action.payload)
      userProfile = yield call(userApi.getUserProfile)
    } catch (error) {
      console.error(error)
      yield put(userActions.loginError(error))
    }
  }

  if (userProfile) {
    yield put(userActions.loginSuccess(userProfile))
  }
}

function* loginFlow() {
  while (true) {
    const task = yield fork(authorize)
    const action = yield take([userActionsTypes.LOGOUT, userActionsTypes.LOGIN_ERROR])

    if (action.type === userActionsTypes.LOGOUT) {
      yield cancel(task)
      yield call(userApi.logout)
    }
  }
}

function* authStateChangeHandler() {
  let prevData = null
  while (true) {
    const data = yield call(userApi.generateAuthStateChangePromise)

    if (prevData && !data) {
      yield put(userActions.logout())
    }
  }
}

export default function* user() {
  yield all([fork(loginFlow), fork(authStateChangeHandler)])
}
