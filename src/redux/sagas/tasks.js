import _ from 'lodash'
import { all, fork, put, call, take, cancel } from 'redux-saga/effects'
import { createPromisesGenerator } from '../../api/utils'
import * as tasksApi from '../../api/tasks'
import { getUserProfile } from '../../api/user'
import { actionTypes as userActionsTypes } from '../modules/user'
import { actionTypes as tasksActionsTypes, actions as tasksActions } from '../modules/tasks'

function* createTask() {
  while (true) {
    const task = yield take(tasksActionsTypes.CREATE)

    try {
      const userId = (yield call(getUserProfile)).uid
      yield call(tasksApi.create, userId, task.payload)
      yield put(tasksActions.createSuccess())
    } catch (error) {
      console.error(error)
      yield put(tasksActions.createError(error))
    }
  }
}

function* updateTask() {
  while (true) {
    const task = yield take(tasksActionsTypes.UPDATE_ITEM)

    try {
      const userId = (yield call(getUserProfile)).uid
      yield call(tasksApi.update, userId, _.get(task, 'payload.key'), _.get(task, 'payload.updates'))
      yield put(tasksActions.updateItemSuccess())
    } catch (error) {
      console.error(error)
      yield put(tasksActions.updateItemError(error))
    }
  }
}

function* removeTask() {
  while (true) {
    const task = yield take(tasksActionsTypes.REMOVE)

    try {
      const userId = (yield call(getUserProfile)).uid
      yield call(tasksApi.remove, userId, task.payload)
      yield put(tasksActions.removeSuccess())
    } catch (error) {
      console.error(error)
      yield put(tasksActions.removeError(error))
    }
  }
}

function* tasksListUpdatesHandler(promiseGenerator) {
  while (true) {
    const tasks = yield promiseGenerator.getPromise()
    yield put(tasksActions.updateList(tasks))
  }
}

function* tasksListUpdatesFlow() {
  while (true) {
    yield take(userActionsTypes.LOGIN_SUCCESS)

    const userId = (yield call(getUserProfile)).uid
    const promiseGenerator = yield call(createPromisesGenerator,(data) => data)
    const cancelListenTasksUpdates = yield call(tasksApi.onUpdateTasks, promiseGenerator.generator, userId)
    const handlerTask = yield fork(tasksListUpdatesHandler, promiseGenerator)

    yield take(userActionsTypes.LOGOUT)
    yield cancel(handlerTask)
    yield call(cancelListenTasksUpdates)
  }
}

export default function* user() {
  yield all([
    fork(tasksListUpdatesFlow),
    fork(updateTask),
    fork(createTask),
    fork(removeTask)]
  )
}
