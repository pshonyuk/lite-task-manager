import _ from 'lodash'
import { applyMiddleware, createStore, compose } from 'redux'
import { createLogger } from 'redux-logger'
import createSagaMiddleware from 'redux-saga'
import rootReducer from './modules'
import rootSaga from './sagas'

function getDevToolsExtension({ isDebug }) {
  if (isDebug) {
    if (_.isFunction(window.devToolsExtension)) {
      return window.devToolsExtension()
    }
    console.warn('Please install Redux DevTools for your browser')
  }
}

function getMiddlewares({ isDebug }) {
  const middlewares = {
    saga: createSagaMiddleware(),
  }

  if (isDebug) {
    middlewares.logger = createLogger({
      collapsed: true,
      logErrors: false,
      level: action => (action.error ? 'warn' : 'log'),
    })
  }

  return middlewares
}

export default function configureStore(initialState = {}) {
  const isDebug = process.env.NODE_ENV === 'development'
  const middlewares = getMiddlewares({ isDebug })
  const devToolsExtension = getDevToolsExtension({ isDebug })
  const enhancer = compose(
    ...[applyMiddleware(..._.values(middlewares)), devToolsExtension].filter(_.isFunction)
  )

  return {
    ...createStore(rootReducer, initialState, enhancer),
    runSaga: (...args) => middlewares.saga.run(rootSaga, ...args),
  }
}
