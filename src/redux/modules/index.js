import { combineImmutableReducers } from 'immutable-redux'
import user from './user'
import tasks from './tasks'
import exportData from './exportData'

export default combineImmutableReducers({
  user,
  tasks,
  exportData,
})
