import { Record } from 'immutable'

export default Record({
  key: null,
  name: '',
  description: '',
  imageUrl: '',
  favorite: false,
  createdAt: null,
  updatedAt: null,
})
