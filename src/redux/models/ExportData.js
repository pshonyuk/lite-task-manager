import { Record, Map } from 'immutable'

export default Record({
  inProgress: new Map(),
  errors: new Map(),
})
