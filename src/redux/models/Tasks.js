import { Record, Map, List, Set } from 'immutable'

export default Record({
  data: new List(),
  selected: new Set(),
  inProgress: new Map(),
  errors: new Map(),
})
