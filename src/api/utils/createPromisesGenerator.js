export default (callback) => {
  const promiseGenerator = {
    promise: null,
    resolve: () => null,
    reject: () => null,
    next() {
      this.promise = new Promise((resolve, reject) => {
        this.resolve = resolve
        this.reject = reject
      })
    },
  }

  promiseGenerator.next()

  return {
    generator: (...args) => {
      try {
        promiseGenerator.resolve(callback(...args))
      } catch (error) {
        promiseGenerator.reject(error)
      } finally {
        promiseGenerator.next()
      }
    },
    getPromise: () => promiseGenerator.promise
  }
}
