import * as firebase from 'firebase'
import { createPromisesGenerator } from './utils'
import { authProviders } from './constants'

const providers = {
  [authProviders.GOOGLE]: new firebase.auth.GoogleAuthProvider(),
  [authProviders.FACEBOOK]: new firebase.auth.FacebookAuthProvider(),
  [authProviders.GITHUB]: new firebase.auth.GithubAuthProvider(),
  [authProviders.TWITTER]: new firebase.auth.TwitterAuthProvider(),
}

export function login(providerType) {
  const provider = providers[providerType]
  return firebase
    .auth()
    .signInWithPopup(provider)
    .then(result => result.user)
}

export function logout() {
  return firebase.auth().signOut()
}

export function getUserProfile() {
  return firebase.auth().currentUser || null
}

export const generateAuthStateChangePromise = (() => {
  const promiseGenerator = createPromisesGenerator((data) => data)
  firebase.auth().onAuthStateChanged(promiseGenerator.generator)

  return promiseGenerator.getPromise
})()
