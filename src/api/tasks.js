import _ from 'lodash'
import * as firebase from 'firebase'

function getTaskEntryPoint(userId, taskKey) {
  let entryPointParts = ['tasks']

  if (userId) {
    entryPointParts.push(userId)
    taskKey && entryPointParts.push(taskKey)
  }

  return entryPointParts.join('/')
}

export function create(userId, body = {}) {
  if (!userId) {
    throw new Error('Invalid userId')
  }

  const data = {
    name: body.name,
    description: body.description || '',
    imageUrl: body.imageUrl || null,
    favorite: false,
    createdAt: Date.now(),
    updatedAt: Date.now(),
  }

  return firebase
    .database()
    .ref(getTaskEntryPoint(userId))
    .push(data)
}

export function update(userId, key, updates = {}) {
  if (!userId || !key) {
    throw new Error('Invalid userId or key')
  }

  const data = {
    ..._.pick({
      name: updates.name,
      description: updates.description || '',
      imageUrl: updates.imageUrl || null,
      favorite: updates.favorite,
    }, _.keys(updates)),
    updatedAt: Date.now(),
  }

  return firebase
    .database()
    .ref(getTaskEntryPoint(userId, key))
    .update(data)
}

export function remove(userId = 0, keys) {
  if (!userId || !keys) {
    throw new Error('Invalid userId or key')
  }

  const ref = firebase
    .database()
    .ref(getTaskEntryPoint(userId))

  return Promise.all(_.castArray(keys).map((key) => {
    return ref.child(key).remove()
  }))
}

export function onUpdateTasks(callback, userId = 0) {
  const eventHandler = (snapshot) => {
    const tasksMap = _.mapValues(snapshot.val(), (task, key) => _.assign({}, task, { key }))
    const actuallyData = _.values(tasksMap).filter((t = {}) => t.key && t.name)
    setTimeout(() => callback(actuallyData), 0)
  }

  const ref = firebase
    .database()
    .ref(getTaskEntryPoint(userId))

  ref.on('value', eventHandler)

  return () => ref.off('value', eventHandler)
}

