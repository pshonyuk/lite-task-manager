export const authProviders = {
  GOOGLE: 'google',
  FACEBOOK: 'facebook',
  GITHUB: 'github',
  TWITTER: 'twitter',
}
