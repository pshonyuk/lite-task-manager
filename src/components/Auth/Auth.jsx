import React from 'react'
import PropTypes from 'prop-types'
import { authProviders } from '../../api/constants'
import { LinearProgress } from 'material-ui/Progress'
import Grid from 'material-ui/Grid'
import Avatar from 'material-ui/Avatar'
import Button from 'material-ui/Button'
import Typography from 'material-ui/Typography'
import Dialog, { DialogContent, DialogTitle } from 'material-ui/Dialog'
import authContainer from './authContainer'
import styles from './Auth.scss'

const providers = [
  {
    provider: authProviders.GOOGLE,
    iconSrc:
      'https://static-assets.filmfreeway.com/assets/google_icon-ed3d9227111f14d90a5d4c0c8f7d39be.svg',
    caption: 'Login with Google',
  },
  {
    provider: authProviders.FACEBOOK,
    iconSrc: 'https://seeklogo.com/images/F/facebook-icon-logo-03865A9BA2-seeklogo.com.png',
    caption: 'Login with Facebook',
  },
  {
    provider: authProviders.GITHUB,
    iconSrc: 'https://image.flaticon.com/icons/svg/25/25231.svg',
    caption: 'Login with Github',
  },
  {
    provider: authProviders.TWITTER,
    iconSrc: 'http://www.qcode.in/wp-content/themes/qcode1/assets/images/icon-twitter-logo.svg',
    caption: 'Login with Twitter',
  },
]

class Auth extends React.Component {
  static propTypes = {
    isAuthPreparing: PropTypes.bool.isRequired,
    isLogin: PropTypes.bool.isRequired,
    login: PropTypes.func.isRequired,
    logout: PropTypes.func.isRequired,
  }

  loginByProvider = provider => {
    this.props.login(provider)
  }

  renderProviders() {
    return (
      <Grid container spacing={24}>
        {providers.map(data => (
          <Grid key={data.provider} xs={12} item>
            <Button
              className={styles.providerButton}
              onClick={() => this.loginByProvider(data.provider)}
              fullWidth
              raised
            >
              <Avatar src={data.iconSrc} className={styles.providerIcon} />
              <Typography type="title">{data.caption}</Typography>
            </Button>
          </Grid>
        ))}
      </Grid>
    )
  }

  render() {
    return (
      <div>
        <Dialog open={!this.props.isLogin} fullWidth={true} aria-labelledby="auth-dialog-title">
          <DialogTitle id="auth-dialog-title">Login</DialogTitle>
          <DialogContent>
            {this.props.isAuthPreparing ? <LinearProgress /> : this.renderProviders()}
          </DialogContent>
        </Dialog>
      </div>
    )
  }
}

export default authContainer(Auth)
