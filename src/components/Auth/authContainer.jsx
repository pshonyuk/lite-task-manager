import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actions as userActions, selectors as userSelectors } from '../../redux/modules/user'
import { getDisplayName } from '../../utils/react'

export default WrappedComponent => {
  const mapStateToProps = state => {
    return {
      isAuthPreparing: userSelectors.isAuthPreparing(state),
      isLogin: userSelectors.isLogin(state),
    }
  }

  const mapDispatchToProps = dispatch => {
    return {
      login: bindActionCreators(userActions.login, dispatch),
      logout: bindActionCreators(userActions.logout, dispatch),
    }
  }

  function Component(props) {
    return <WrappedComponent {...props} />
  }

  Component.displayName = `authContainer${getDisplayName(WrappedComponent)}`

  return connect(mapStateToProps, mapDispatchToProps)(Component)
}
