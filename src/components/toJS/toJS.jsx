import _ from 'lodash'
import React from 'react'
import { Iterable } from 'immutable'
import hoistNonReactStatic from 'hoist-non-react-statics'
import { getDisplayName } from '../../utils/react'

const toPlainJS = props => {
  return _.mapValues(props, v => (Iterable.isIterable(v) ? v.toJS() : v))
}

export default WrappedComponent => {
  class ToJS extends React.Component {
    static displayName = `toJS(${getDisplayName(WrappedComponent)})`

    constructor(props) {
      super(props)
      this.state = { plainProps: toPlainJS(this.props) }
    }

    componentWillReceiveProps(nextProps) {
      const stillKeys = _.keys(_.pickBy(nextProps, (v, k) => v === this.props[k]))
      const changedPlainProps = toPlainJS(_.omit(nextProps, stillKeys))
      const plainProps = { ...changedPlainProps, ..._.pick(this.state.plainProps, stillKeys) }

      this.setState({ plainProps })
    }

    render() {
      return <WrappedComponent {...this.state.plainProps} />
    }
  }

  return hoistNonReactStatic(ToJS, WrappedComponent)
}
