import _ from 'lodash'
import React from 'react'
import PropTypes from 'prop-types'
import Menu, { MenuItem } from 'material-ui/Menu'
import Avatar from 'material-ui/Avatar'
import IconButton from 'material-ui/IconButton'
import AccountCircle from 'material-ui-icons/AccountCircle'
import userProfileContainer from './userProfileMenuContainer'

class UserProfile extends React.Component {
  static propTypes = {
    userProfile: PropTypes.object.isRequired,
    isLogin: PropTypes.bool.isRequired,
    logout: PropTypes.func.isRequired,
    exportTasks: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = { menuAnchorEl: null }
  }

  openMenu = e => {
    this.setState({ menuAnchorEl: e.currentTarget })
  }

  closeMenu = () => {
    this.setState({ menuAnchorEl: null })
  }

  logout = () => {
    this.closeMenu()
    this.props.logout()
  }

  exportTasks = () => {
    this.closeMenu()
    this.props.exportTasks()
  }

  displayUserName() {
    return _.get(this.props.userProfile, 'displayName', 'Nobody')
  }

  render() {
    if (!this.props.isLogin) {
      return null
    }

    const isOpen = !!this.state.menuAnchorEl
    return (
      <div>
        <IconButton
          aria-owns={isOpen ? 'menu-appbar' : null}
          aria-haspopup="true"
          onClick={this.openMenu}
        >
          {this.props.userProfile.photoURL ? (
            <Avatar src={this.props.userProfile.photoURL}/>
          ) : (
            <AccountCircle />
          )}
        </IconButton>
        <Menu
          id="menu-appbar"
          anchorEl={this.state.menuAnchorEl}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          open={isOpen}
          onClose={this.closeMenu}
        >
          <MenuItem disabled>Signed in as {this.displayUserName()}</MenuItem>
          <MenuItem onClick={this.exportTasks}>Export to JSON</MenuItem>
          <MenuItem onClick={this.logout}>Logout</MenuItem>
        </Menu>
      </div>
    )
  }
}

export default userProfileContainer(UserProfile)
