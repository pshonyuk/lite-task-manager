import _ from 'lodash'
import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actions as userActions, selectors as userSelectors } from '../../redux/modules/user'
import { selectors as tasksSelectors } from '../../redux/modules/tasks'
import { actions as exportActions } from '../../redux/modules/exportData'
import { getDisplayName } from '../../utils/react'
import toJS from '../toJS'

export default WrappedComponent => {
  const mapStateToProps = state => {
    return {
      userProfile: userSelectors.profile(state),
      isLogin: userSelectors.isLogin(state),
      tasks: tasksSelectors.tasks(state),
    }
  }

  const mapDispatchToProps = dispatch => {
    return {
      logout: bindActionCreators(userActions.logout, dispatch),
      exportDataToFile: bindActionCreators(exportActions.exportDataToFile, dispatch),
    }
  }

  function Component(props) {
    function exportTasks() {
      props.exportDataToFile(props.tasks.map((t) => _.omit(t, ['key'])))
    }

    return <WrappedComponent {...props} exportTasks={exportTasks}/>
  }

  Component.displayName = `userProfileMenuContainer${getDisplayName(WrappedComponent)}`

  return connect(mapStateToProps, mapDispatchToProps)(toJS(Component))
}
