import React from 'react'
import PropTypes from 'prop-types'
import UiAppBar from 'material-ui/AppBar'
import Toolbar from 'material-ui/Toolbar'
import { LinearProgress } from 'material-ui/Progress'
import Typography from 'material-ui/Typography'
import Button from 'material-ui/Button'
import IconButton from 'material-ui/IconButton'
import AddIcon from 'material-ui-icons/Add'
import DeleteIcon from 'material-ui-icons/Delete'
import UserProfileMenu from '../UserProfileMenu'
import { TaskCreateModal } from '../TaskDetailsModal'
import appBarContainer from './appBarContainer'
import styles from './AppBar.scss'

class AppBar extends React.Component {
  static propTypes = {
    selectedTasksKeys: PropTypes.arrayOf(PropTypes.string).isRequired,
    isInProgress: PropTypes.bool.isRequired,
    removeTask: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = {
      createTaskModalIsOpen: false,
      userProfileMenuOpen: false,
    }
  }

  toggleAddTaskModal = () => {
    this.setState(state => ({
      createTaskModalIsOpen: !state.createTaskModalIsOpen,
    }))
  }

  onRemoveSelectedTasksClick = () => {
    this.props.removeTask(this.props.selectedTasksKeys)
  }

  render() {
    return (
      <div className={styles.appContainer}>
        <UiAppBar position="fixed">
          <Toolbar>
            <UserProfileMenu />
            <Typography type="title" color="inherit" className={styles.appBarTitle}>
              Lite Task Manager
            </Typography>
            {!!this.props.selectedTasksKeys.length && (
              <IconButton color='contrast' aria-label="Remove" onClick={this.onRemoveSelectedTasksClick}>
                <DeleteIcon />
              </IconButton>
            )}
            <Button fab mini color="accent" aria-label="add" onClick={this.toggleAddTaskModal}>
              <AddIcon />
            </Button>
          </Toolbar>
          {this.props.isInProgress && (
            <LinearProgress mode="indeterminate" />
          )}
        </UiAppBar>
        {this.state.createTaskModalIsOpen && (
          <TaskCreateModal
            isOpen={this.state.createTaskModalIsOpen}
            toggle={this.toggleAddTaskModal}
          />
        )}
      </div>
    )
  }
}

export default appBarContainer(AppBar)
