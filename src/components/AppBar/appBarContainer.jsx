import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actions as tasksActions, selectors as tasksSelectors } from '../../redux/modules/tasks'
import { getDisplayName } from '../../utils/react'
import toJS from '../toJS'

export default WrappedComponent => {
  const mapStateToProps = state => {
    return {
      isInProgress: tasksSelectors.isInProgress(state),
      selectedTasksKeys: tasksSelectors.getSelectedTasksKeys(state),
    }
  }

  const mapDispatchToProps = dispatch => {
    return {
      removeTask: bindActionCreators(tasksActions.remove, dispatch),
    }
  }

  function Component(props) {
    return <WrappedComponent {...props} />
  }

  Component.displayName = `appBarContainer${getDisplayName(WrappedComponent)}`

  return connect(mapStateToProps, mapDispatchToProps)(toJS(Component))
}
