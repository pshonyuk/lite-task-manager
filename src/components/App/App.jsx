import React from 'react'
import Auth from '../Auth'
import AppBar from '../AppBar'
import TasksGrid from '../TasksGrid'
import styles from './app.scss'

export default class App extends React.Component {
  render() {
    return (
      <div>
        <AppBar isInProgress={this.props.isInProgress} />
        <Auth />
        <div className={styles.contentContainer}>
          <TasksGrid />
        </div>
      </div>
    )
  }
}

