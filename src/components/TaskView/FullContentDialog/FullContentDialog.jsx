import React from 'react'
import PropTypes from 'prop-types'
import Button from 'material-ui/Button'
import Card, { CardMedia } from 'material-ui/Card'
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog'
import TaskShape from '../../../shapes/Task'
import styles from './FullContentDialog.scss'

export default class FullContentDialog extends React.Component {
  static propTypes = {
    task: TaskShape.isRequired,
    isOpen: PropTypes.bool.isRequired,
    toggle: PropTypes.func.isRequired,
  }

  render() {
    return (
      <Dialog
        open={this.props.isOpen}
        onClose={this.props.toggle}
        aria-labelledby="full-content-dialog-title"
      >
        <DialogTitle id="full-content-dialog-title">{this.props.task.name}</DialogTitle>
        <DialogContent>
          {this.props.task.imageUrl && (
            <Card>
              <CardMedia
                image={this.props.task.imageUrl}
                title={this.props.task.name}
                className={styles.taskViewCardMedia}
              />
            </Card>
          )}
          <DialogContentText>
            {this.props.task.description}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.props.toggle} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}
