import _ from 'lodash'
import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import getRandomImageUrl from '../../api/getRandomImageUrl'
import TaskModel from '../../redux/models/Task'
import { actions as tasksActions } from '../../redux/modules/tasks'
import { getDisplayName } from '../../utils/react'
import toJS from '../toJS'
import TaskDetailsModal from './TaskDetailsModal'

class TaskEditModalContainer extends React.Component {
  static displayName = `taskCreateModalContainer${getDisplayName(TaskDetailsModal)}`

  static propTypes = {
    isOpen: PropTypes.bool.isRequired,
    toggle: PropTypes.func.isRequired,
    createTask: PropTypes.func,
  }

  createTask = (data = {}) => {
    this.props.toggle()
    this.props.createTask(data)
  }

  render() {
    return (
      <TaskDetailsModal
        {...this.props}
        title="Create Task"
        addButtonText="Create"
        cancelButtonText="Cancel"
        onEdit={this.createTask}
      />
    )
  }
}

const mapStateToProps = () => {
  return {
    task: new TaskModel({
      key: _.uniqueId('task'),
      imageUrl: getRandomImageUrl(),
    }),
  }
}

const mapDispatchToProps = dispatch => {
  return {
    createTask: bindActionCreators(tasksActions.create, dispatch),

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(toJS(TaskEditModalContainer))
