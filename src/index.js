import './api/firebase'
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { BrowserRouter, Route } from 'react-router-dom'
import { fromJS } from 'immutable'
import App from './components/App'
import registerServiceWorker from './registerServiceWorker'
import configureStore from './redux/configureStore'

const initialState = fromJS({})
const store = configureStore(initialState)
store.runSaga()

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Route path="/" component={App} />
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
)

registerServiceWorker()
