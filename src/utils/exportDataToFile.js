export default (data, fileName = 'export', metadata = 'data:text/json;charset=utf-8') => {
  const dataStr = `${metadata},${encodeURIComponent(JSON.stringify(data, null, 4))}`
  const downloadAnchorNode = document.createElement('a')
  downloadAnchorNode.setAttribute('href', dataStr)
  downloadAnchorNode.setAttribute('download', `${fileName}.json`)
  downloadAnchorNode.click()
  downloadAnchorNode.remove()
}
