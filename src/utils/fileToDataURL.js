import _ from 'lodash'

export default (file) => {
  let reader  = new FileReader()

  const clearReader = () => {
    reader.onloadend = null
    reader = null
  }

  return (new Promise((resolve) => {
    reader.onloadend = () => resolve(reader.result)

    reader.readAsDataURL(_.isArrayLike(file) ? file[0] : file)
  })).then(
    (data) => {
      clearReader()
      return data
    },
    error => {
      clearReader()
      return error
    }
  )
}
